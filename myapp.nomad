job "myapp" {
  datacenters = ["dc1"]
  type        = "service"

  group "myapp" {
    count = 3
    network {
      port "http" { to = 5000 }
    }
    service {
      name = "myapp"
      tags = ["urlprefix-/"]
      port = "http"
      check {
        type     = "http"
        port     = "http"
        path     = "/"
        interval = "10s"
        timeout  = "2s"
      }
    }
    task "myapp" {
      driver = "docker"
      config {
        image = "seymurnet/myapp:latest"
        ports = ["http"]
      }
    }

  }
}
