job "hello-world" {
  datacenters = ["dc1"]
  group "hello-world" {
    network {
      port "http" { to = 5000 }
    }
    service {
      name = "hello-world"
      tags = ["urlprefix-internetarchive-bai-master.x.archive.org:443/"]
      port = "http"
      check {
        type     = "http"
        port     = "http"
        path     = "/"
        interval = "10s"
        timeout  = "2s"
      }
    }
    task "hello-world" {
      driver = "docker"
      config {
        image = "registry.gitlab.com/internetarchive/bai/master:latest"
        ports = [ "http" ]
      }
    }
  }
}
